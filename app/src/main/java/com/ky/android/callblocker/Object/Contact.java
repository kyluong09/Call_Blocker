package com.ky.android.callblocker.Object;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by kyluong09 on 12/2/17.
 */

public class Contact extends RealmObject {
    @PrimaryKey
    String _id;
    String phoneNumber;

    public Contact (){
        _id = UUID.randomUUID().toString();
    }

    public Contact( String phoneNumber){
        _id = UUID.randomUUID().toString();
        this.phoneNumber = phoneNumber;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
