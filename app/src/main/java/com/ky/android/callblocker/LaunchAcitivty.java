package com.ky.android.callblocker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LaunchAcitivty extends AppCompatActivity {
    Button startButton;


    private void findViewID(){
        startButton = (Button) findViewById(R.id.startButton);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        // Find View Id
        findViewID();

        // Init intent to start a new activity

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LaunchAcitivty.this,MainActivity.class);
                startActivity(intent);
            }
        });

    }
}
