package com.ky.android.callblocker.Object;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by kyluong09 on 12/9/17.
 */

public class PhoneNumber extends RealmObject {
    @PrimaryKey
    String _id;
    @Required
    String phoneNumber;

    public PhoneNumber(){
        _id = UUID.randomUUID().toString();
    }

    public PhoneNumber(String phoneNumber){
        _id = UUID.randomUUID().toString();
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
