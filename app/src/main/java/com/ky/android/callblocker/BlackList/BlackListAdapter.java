package com.ky.android.callblocker.BlackList;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ky.android.callblocker.EditActivity;
import com.ky.android.callblocker.MainActivity;
import com.ky.android.callblocker.Object.PhoneNumber;
import com.ky.android.callblocker.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

import io.realm.Realm;

/**
 * Created by kyluong09 on 12/9/17.
 */

public class BlackListAdapter extends BaseAdapter {
    Realm mRealm;
    Context context;
    LayoutInflater inflater;
    ArrayList<PhoneNumber> blackList;
    ///////////XML Comp////////////
    TextView phoneNumber;
    ImageButton deleteButton;
    ImageButton editButton;
    String newPhoneNumber;

    public BlackListAdapter (Context context, ArrayList<PhoneNumber> list){
        this.context = context;
        this.blackList = list;
        this.inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return this.blackList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.blackList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if(view == null ) {
            view = LayoutInflater.from(context).inflate(R.layout.view_black_list, viewGroup, false);
        }
        phoneNumber = (TextView) view.findViewById(R.id.phone);
        phoneNumber.setText(blackList.get(i).getPhoneNumber());

        // Delete button
        deleteButton = (ImageButton) view.findViewById(R.id.delete_black);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get current number
                final String currentNum = blackList.get(i).getPhoneNumber();
                mRealm = Realm.getDefaultInstance();
                // remove number at current position
                blackList.remove(i);
                // remove current number in realm database
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        PhoneNumber temp = realm.where(PhoneNumber.class).equalTo("phoneNumber",currentNum).findFirst();
                        temp.deleteFromRealm();
                    }
                });
                notifyDataSetChanged();
            }
        });






        // Edit button
        editButton = (ImageButton) view.findViewById(R.id.edit_button);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,EditActivity.class);
                intent.putExtra("id",blackList.get(i).get_id());
                context.startActivity(intent);
            }
        });


        return view;
    }
}
