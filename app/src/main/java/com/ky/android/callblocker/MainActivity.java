package com.ky.android.callblocker;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BlockedNumberContract;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.TelecomManager;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.ky.android.callblocker.BlackList.BlackListActivity;
import com.ky.android.callblocker.Object.Contact;
import com.ky.android.callblocker.Object.Mode;
import com.ky.android.callblocker.Object.PhoneNumber;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {
    // Context
    Context context;
    //Realm Init
    Realm mRealm;
    // XML Comp
    RadioGroup radioGroup;
    RadioButton buttonList;
    RadioButton buttonContact;
    RadioButton buttonAll;
    // Input
    String phoneNumber;
    // Telemanager
    TelecomManager telecomManager;


    private void findViewId(){
        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        buttonList = (RadioButton) findViewById(R.id.radio_button_black);
        buttonContact = (RadioButton) findViewById(R.id.radio_button_contact);
        buttonAll = (RadioButton) findViewById(R.id.radio_button_all);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId() == R.id.add_number){
            // Get view layout
            final LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
            View view = inflater.inflate(R.layout.input_dialog,null);
            // Set alert dialog
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(view);

            // Input
            final EditText input = (EditText) view.findViewById(R.id.phone_input);
            input.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
            builder.setCancelable(true)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Parse input string into integer
                            try{
                                phoneNumber = input.getText().toString();


                            }catch(NumberFormatException e){
                                Log.i("Tag","This not a phone number");

                            }



                            // get realm instance
                            mRealm = Realm.getDefaultInstance();
                            mRealm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    PhoneNumber num = new PhoneNumber(phoneNumber);
                                    realm.copyToRealmOrUpdate(num);
                                }
                            }, new Realm.Transaction.OnSuccess() {
                                @Override
                                public void onSuccess() {
                                    // Transaction was a success
                                    Log.i("Tag","Realm Path =" +mRealm.getPath());
                                    Toast.makeText(MainActivity.this,"Added Successful",Toast.LENGTH_SHORT).show();
                                }
                            }, new Realm.Transaction.OnError() {
                                @Override
                                public void onError(Throwable error) {
                                    // Transaction failed and was automatically canceled.
                                    Toast.makeText(MainActivity.this,"Added Failed",Toast.LENGTH_SHORT).show();
                                }
                            });



                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();

                        }
                    });



            AlertDialog alert = builder.create();
            alert.show();

            return true;
        }
        else if(item.getItemId() == R.id.black_list){
            startActivity(new Intent(MainActivity.this, BlackListActivity.class));
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.list,menu);
        return true;
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Find View ID
        findViewId();
        mRealm = Realm.getDefaultInstance();
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(Contact.class);
            }
        });

        // Get contacts from the phone and store in realm
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        while (phones.moveToNext())
        {
            final Contact contact = new Contact();
            contact.setPhoneNumber(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
            Log.i("Tag","Contact = " + contact.getPhoneNumber());
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(contact);
                }
            });

        }
        phones.close();




        // Radio group and radio button
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(radioGroup.getCheckedRadioButtonId() == R.id.radio_button_black){
                    mRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.delete(Mode.class);
                            Mode mode = new Mode("1","list");
                            realm.copyToRealmOrUpdate(mode);
                        }
                    });
                }
                else if(radioGroup.getCheckedRadioButtonId() == R.id.radio_button_contact){
                    mRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.delete(Mode.class);
                            Mode mode = new Mode("2","contact");
                            realm.copyToRealmOrUpdate(mode);
                        }
                    });

                }else if (radioGroup.getCheckedRadioButtonId() == R.id.radio_button_all){
                    mRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.delete(Mode.class);
                            Mode mode = new Mode("3","all");
                            realm.copyToRealmOrUpdate(mode);
                        }
                    });


                }
                else{
                    mRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.delete(Mode.class);
                            Mode mode = new Mode("4","none");
                            realm.copyToRealmOrUpdate(mode);
                        }
                    });

                }


            }





        });












    }

    /////////////////////////////////METHODS//////////////////////////////////


}
