package com.ky.android.callblocker.Block;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.telecom.TelecomManager;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.ky.android.callblocker.Object.Contact;
import com.ky.android.callblocker.Object.Mode;
import com.ky.android.callblocker.Object.PhoneNumber;

import java.lang.reflect.Method;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

import static android.content.Context.TELEPHONY_SERVICE;


/**
 * Created by kyluong09 on 12/9/17.
 */

public class BlockActivity extends BroadcastReceiver {
    Realm mRealm;
    // black list number
    ArrayList<String> phoneList = new ArrayList<>();
    // contact number
    ArrayList<String> contactList = new ArrayList<>();
    ArrayList<String> state = new ArrayList<>();


    @Override
    public void onReceive(final Context context, final Intent intent) {
        mRealm = Realm.getDefaultInstance();
        // Store black list into arraylist
        RealmResults<PhoneNumber> phoneResult = mRealm.where(PhoneNumber.class).findAll();
        for(PhoneNumber phon:phoneResult){
            phoneList.add(phon.getPhoneNumber());
        }
        // Store contacts into arraylist
        RealmResults<Contact> contactResult = mRealm.where(Contact.class).findAll();
        for(Contact con:contactResult){
            String formatNumber = PhoneNumberUtils.formatNumber(con.getPhoneNumber(),"US");
            contactList.add(formatNumber);

        }


        if (intent.getAction().equals("android.intent.action.PHONE_STATE")){
            final String numberCall = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);;
            // get mode
            RealmResults<Mode> mode = mRealm.where(Mode.class).findAll();
            for(Mode mo: mode){
                state.add(mo.getMode());
            }


            //Log.i("Tag", "Mode = " + mode.get_id());
            if(state.contains("list")){


                if(phoneList.contains(numberCall)){
                    endCall(context);
                }

            }
            else if(state.contains("contact")){
                String format = PhoneNumberUtils.formatNumber(numberCall,"US");
                if(!contactList.contains(format)){
                    endCall(context);
                }

            }
            else if(state.contains("all")){
                endCall(context);
            }
            else if(state.contains("none")){

            }

        }
    }

    // Keep this method as it is
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void endCall(Context context) {
        try {

            TelephonyManager manager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
            Method iTeMethod = manager.getClass().getDeclaredMethod("getITelephony");
            iTeMethod.setAccessible(true);
            Object teleObject= iTeMethod.invoke(manager);
            Method siLent = teleObject.getClass().getDeclaredMethod("silenceRinger");
            Method endCall = teleObject.getClass().getDeclaredMethod("endCall");
            siLent.invoke(teleObject);
            endCall.invoke(teleObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
