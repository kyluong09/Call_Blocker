package com.ky.android.callblocker.RealmDB;

import android.app.Application;
import android.util.Log;

import com.ky.android.callblocker.Object.Mode;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by kyluong09 on 12/2/17.
 */

public class RealmApplication extends Application {
    Realm mRealm;
    private static RealmApplication instance;
    private static RealmApplication getInstance(){
        return instance;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Realm.init(getApplicationContext());
        RealmConfiguration config = new RealmConfiguration.Builder().name("Call_Blocker").deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(config);

    }





}
