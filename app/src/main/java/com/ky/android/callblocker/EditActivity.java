package com.ky.android.callblocker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ky.android.callblocker.BlackList.BlackListActivity;
import com.ky.android.callblocker.Object.PhoneNumber;

import io.realm.Realm;

public class EditActivity extends AppCompatActivity {
    Button save;
    Button cancel;
    EditText input;
    String newPhone;
    String currentPhone;
    // Realm
    Realm mRealm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        // get current phone
        currentPhone = getIntent().getExtras().getString("id");



        input = ( EditText) findViewById(R.id.input_new_number);
        input.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                newPhone = input.getText().toString();

                // get default realm
                mRealm = Realm.getDefaultInstance();
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        PhoneNumber temp = realm.where(PhoneNumber.class).equalTo("_id",currentPhone).findFirst();
                        temp.setPhoneNumber(newPhone);
                        realm.copyToRealmOrUpdate(temp);

                    }
                });


                startActivity(new Intent(EditActivity.this, BlackListActivity.class));


            }
        });

        cancel = (Button) findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(EditActivity.this, BlackListActivity.class));


            }
        });
    }
}
