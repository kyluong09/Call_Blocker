package com.ky.android.callblocker.Object;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by kyluong09 on 12/10/17.
 */

public class Mode extends RealmObject{
    @PrimaryKey
    String _id;
    @Required
    String mode;

    public Mode(){
    }

    public Mode(String _id,String mode){
        this._id = _id;
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
