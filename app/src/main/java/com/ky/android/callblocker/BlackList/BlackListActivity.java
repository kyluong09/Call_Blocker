package com.ky.android.callblocker.BlackList;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.ky.android.callblocker.Object.PhoneNumber;
import com.ky.android.callblocker.R;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class BlackListActivity extends AppCompatActivity {
    Realm mRealm;
    // Base adapter
    BlackListAdapter adapter;
    // Xml comp
    ListView listView;
    // Arraylist of phone number
    ArrayList<PhoneNumber> phoneList = new ArrayList<>();


    private void findById(){
        listView = (ListView) findViewById(R.id.list);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_black_list);
        //get realm instance
        mRealm = Realm.getDefaultInstance();
        findById();

        //Get blacklist from realm database
        RealmResults result = mRealm.where(PhoneNumber.class).findAll();
        phoneList.addAll(mRealm.copyFromRealm(result));
        for(int i = 0; i < phoneList.size(); i++){
            Log.i("Tag","phoneList = " + phoneList.get(i).getPhoneNumber());
        }


        if(adapter == null){
            adapter = new BlackListAdapter(getApplicationContext(), phoneList);
        }

        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();






    }
}
